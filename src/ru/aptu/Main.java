/**
 * Main.java
 */

package ru.aptu;

import java.io.EOFException;
import java.io.FileNotFoundException;
import java.io.IOException;
import ru.aptu.facedetector.classifier.ClassifierCalculator;
import ru.aptu.facedetector.classifier.teaching.ImageBundleLoader;
import ru.aptu.facedetector.classifier.teaching.ImageFile;
import ru.aptu.facedetector.haar.*;
/**
 * This is the class containing an entry point.
 */
public class Main { 
	
	/**
	 * The entry point for an application. 
	 * @param   args  The array of an argument strings.
	 */
	public static void main(String[] args) { 
		if (args.length == 0) {
			System.out.println("Error: not enough parameters.\nUsage: java BMPLoader filename");
			return; 
		} 
		try { 
			ImageBundleLoader imageBundleLoader = new ImageBundleLoader(); 
			ImageFile[] images = imageBundleLoader.load(args[0]); 
			
			HaarFeatureCalculator[] haarFeatures = new HaarFeatureCalculator[5]; 
			
			int tmpThreshold = 6000000; 
			
			haarFeatures[0] = new HaarY1X2(images[0].getIntegralImage().getHeight() - 1, 
					images[0].getIntegralImage().getWidth() - 1, tmpThreshold);
			haarFeatures[1] = new HaarY2X1(images[0].getIntegralImage().getHeight() - 1, 
					images[0].getIntegralImage().getWidth() - 1, tmpThreshold);
			haarFeatures[2] = new HaarY3X1(images[0].getIntegralImage().getHeight() - 1, 
					images[0].getIntegralImage().getWidth() - 1, tmpThreshold);
			haarFeatures[3] = new HaarY1X3(images[0].getIntegralImage().getHeight() - 1, 
					images[0].getIntegralImage().getWidth() - 1, tmpThreshold);
			haarFeatures[4] = new HaarY1X2(images[0].getIntegralImage().getHeight() - 1, 
					images[0].getIntegralImage().getWidth() - 1, tmpThreshold);
			
			ClassifierCalculator calc = new ClassifierCalculator(); 
			int[] classifierIndexes = calc.compute(images, haarFeatures, 0, 0, 
					images[0].getIntegralImage().getHeight(), 
					images[0].getIntegralImage().getWidth()); 
			
			System.out.println("\nEps: "); 
			for (int i = 0; i < classifierIndexes.length; i++) { 
				System.out.println("classifierIndexes[" + i + "] = " + classifierIndexes[i]); 
			}
			 
		} catch (FileNotFoundException e) {
			System.out.println("Error: file \"" + args[0] + "\" not found.");
			e.printStackTrace();
		} catch (EOFException e) { 
			System.out.println("Unexpected end of file.");
			System.out.println("Details: \n" + e.getMessage());
			e.printStackTrace();	
		} catch (IOException e) {  
			System.out.println("Not sure if you see this but lol we have a problem with I/O");
			System.out.println("Details: \n" + e.getMessage());
			e.printStackTrace(); 
		} catch (Exception e) {
			System.out.println("Ooops, unhandled exception.");
			System.out.println("Details: \n" + e.getMessage());
			e.printStackTrace(); 
		}
	}
}
