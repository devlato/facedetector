// 
// 

package ru.aptu.facedetector.classifier; 

import java.awt.Rectangle;
import java.io.IOException;
import java.util.ArrayList;

import ru.aptu.facedetector.haar.HaarFeatureCalculator; 
import ru.aptu.facedetector.classifier.teaching.ImageFile; 

public class ClassifierCalculator { 
	
	public ArrayList<Rectangle> findFaces() { 
		ArrayList<Rectangle> detectedFaces = new ArrayList<Rectangle>(); 
		return detectedFaces; 
	}
	
	public int[] computeAll(ImageFile[] images, int y, int x, 
			int windowSizeY, int windowSizeX) { 
		
		return null; 
	}
	
	public int[] compute(ImageFile[] images, HaarFeatureCalculator[] haarFeatures, 
			int y, int x, int windowSizeY, int windowSizeX) throws IOException { 
		
		int weightsCount = images.length; 
		double[] weights = new double[weightsCount];
		int positives = 0;
		int negatives = 0; 
		for (int i = 0; i < weightsCount; i++) { 
			if (images[i].isOK != 0) { 
				positives++; 
			} else { 
				negatives++; 
			}
		}
		
		double sumOfWeights = 0.0f; 
		for (int i = 0; i < weightsCount; i++) { 
			weights[i] = (images[i].isOK == 0) 
					? 1.0f / 2 * negatives
					: 1.0f / 2 * positives; 
		}
		
		double[] eps = new double[haarFeatures.length]; 
		int stepCount = images.length; // temporarily  
		int[] optimalFeatureIndeces = new int[stepCount]; 
		double minEps = Double.MAX_VALUE; 
		for (int t = 0; t < stepCount; t++) { 
			sumOfWeights = 0.0f; 
			for (int i = 0; i < weightsCount; i++) {
				sumOfWeights += weights[i];
			}
			
			System.out.println("\nWeights: {"); 
			for (int i = 0; i < weightsCount; i++) { 
				weights[i] = weights[i] / sumOfWeights; 
				System.out.println(weights[i]); 
			}
			System.out.println("}"); 
			
			int positivelyDetected = 0; 
			// int negativelyDetected = 0; 
			int falsePositivelyDetected = 0;
			int[] wasIncorrectlyClassified = new int[images.length]; 
			for (int f = 0; f < haarFeatures.length; f++) { 
				for (int i = 0; i < images.length; i++) { 
					long featureValue = haarFeatures[f].calculate(0, 0, images[i].getIntegralImage()); 
					System.out.println("(" + t + ", " + f + ", " + i + ") fvalue = " + Math.abs(featureValue) + ", threshold = " + haarFeatures[f].getThreshold()); 
					int featureDetectionResult = (Math.abs(featureValue) <= haarFeatures[f].getThreshold()) 
							? 1 
							: 0; 
					
					double currentEps = weights[i] * Math.abs(featureDetectionResult - images[i].isOK); 
					eps[f] += currentEps; //// 
					
					if (featureDetectionResult != 0) {
						long positiveThreshold = haarFeatures[f].getPositiveThreshold(); 
						long falsePositiveThreshold = haarFeatures[f].getFalsePositiveThreshold();  
						if (currentEps != 0) { 
							positiveThreshold = (positiveThreshold * (positivelyDetected++) + featureValue) / positivelyDetected;  
						} else { 
							falsePositiveThreshold = (falsePositiveThreshold * (falsePositivelyDetected++) + featureValue) / falsePositivelyDetected; 
							wasIncorrectlyClassified[i] = 1; 
						}
						haarFeatures[f].setThreshold(positiveThreshold, falsePositiveThreshold); 
					} //else { 
					//	negativelyDetected++; 
					//} 
				}
				if (eps[f] < minEps) { 	 
					optimalFeatureIndeces[t] = f; 
					minEps = eps[f]; 
				} 
			}
			
			for (int i = 0; i < weightsCount; i++) { 
				weights[i] *= Math.pow(minEps / (1 - minEps), 1 - wasIncorrectlyClassified[i]); 
				System.out.println("image[" + i + "] was incorrectly classified: " + wasIncorrectlyClassified[i]); 
			}
		}
		
		return optimalFeatureIndeces; 
	}
	
}

