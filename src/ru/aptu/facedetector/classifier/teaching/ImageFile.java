// 
// 

package ru.aptu.facedetector.classifier.teaching;

import ru.aptu.facedetector.image.integral.IntegralImage;

public class ImageFile { 
	public int isOK; 
	private IntegralImage integralImage; 
	private String fileName; 
	
	public ImageFile(IntegralImage integralImage, int isOK, String fileName) {
		this.integralImage = integralImage; 
		this.isOK = isOK; 
		this.fileName = fileName; 
	}
	
	public IntegralImage getIntegralImage() {
		return integralImage;
	}
	
	public String toString() { 
		String newLine = System.getProperty("line.separator"); 
		return new String(
			"Image name: " + this.fileName      + newLine + 
			"Content: "    + this.integralImage  
		); 
	}
}