// 
// 

package ru.aptu.facedetector.classifier.teaching;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;

import ru.aptu.facedetector.bitmap.Bitmap;
import ru.aptu.facedetector.bitmap.BitmapLoader;
import ru.aptu.facedetector.image.ImageConverter;
import ru.aptu.facedetector.image.bgra.BGRAImage;
import ru.aptu.facedetector.image.integral.IntegralImage;

public class ImageBundleLoader { 
	
	public ImageFile[] load(String bundleDescriptorfileName) throws IOException, NumberFormatException { 
		BufferedReader reader = new BufferedReader(
				new FileReader(new File(bundleDescriptorfileName)));  
		ImageFile[] imageFiles = null;  
		int fileCount = 0;
		try { 
			fileCount = Integer.parseInt(reader.readLine()); 
			imageFiles = new ImageFile[fileCount]; 
			ImageConverter imageConverter = new ImageConverter(); 
			for (int i = 0; i < fileCount; i++) {
				int isOK = 0; 
				IntegralImage integralImage = null; 
				isOK = Integer.parseInt(reader.readLine()); 
				String text = reader.readLine(); 
				BufferedInputStream input = new BufferedInputStream(
						new FileInputStream(new File(text)));
				Bitmap bitmap = new BitmapLoader().load(input);
				BGRAImage bgraImage = bitmap.getBGRAImage(); 
				integralImage = imageConverter.fromHSVImagetoIntegralImage(
						imageConverter.fromBGRAtoHSV(bgraImage));
				imageFiles[i] = new ImageFile(integralImage, isOK, text); 
			}
			
		} catch (IOException e) { 
			reader.close(); 
			throw e; 
		} catch (NumberFormatException e) { 
			throw e; 
		} finally { 
			reader.close(); 
		}
		
		return imageFiles; 
	}
}
