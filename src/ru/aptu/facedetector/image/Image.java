/**
 * Image.java
 */

package ru.aptu.facedetector.image;

abstract public class Image<T> { 
	private T[][] pixels; 
	
	public Image(T[][] pixels) throws RuntimeException { 
		if (pixels.length != 0) { 
			this.pixels = pixels;
		} else { 
			throw new RuntimeException("Uninitialized array was given to an Image class constructor.");  
		}
	}
	
	public int getHeight() { 
		return this.pixels.length; 
	}
	
	public int getWidth() { 
		return this.pixels[0].length; 
	}
	
	public T[][] getPixels() { 
		return this.pixels; 
	}
	
	public String toString() { 
		String newLine = System.getProperty("line.separator"); 
		return new String( 
			"width: "  + this.getWidth()  + newLine + 
			"height: " + this.getHeight() + newLine
		);  
	}
	
	public void fullPrint() { 
		System.out.println("Full data = {"); 
		for (int y = 0; y < this.getHeight(); y++) { 
			for (int x = 0; x < this.getWidth(); x++) { 
				System.out.print(this.pixels[y][x] + "\t"); 
			}
			System.out.println("");
		}
		System.out.println("}"); 
	}
}
