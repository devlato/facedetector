// 
// 

package ru.aptu.facedetector.image.hsv; 

public class HSVPixel {
	private final short hue; 
	private final short saturation; 
	private final short value; 
	
	public HSVPixel(short hue, short saturation, short value) { 
		this.hue        = hue; 
		this.saturation = saturation; 
		this.value      = value; 
	}
	
	public short getHue() {
		return hue;
	}
	
	public short getSaturation() {
		return saturation;
	}

	public short getValue() {
		return value;
	}
	
	/**
	 * This methods performs a type cast 
	 * from BGRAPixel to String.
	 * @return     The String object containing field values.
	 */
	public String toString() { 
		String string = new String(
			"{"            +
			"Hue: "        + (int) this.hue        + ", " + 
			"Saturation: " + (int) this.saturation + ", " + 
			"Value: "      + (int) this.value      + 
			"}"
		);
		return string;
	}
}
