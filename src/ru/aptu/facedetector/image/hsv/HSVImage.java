/**
 * HSVImage.java
 */

package ru.aptu.facedetector.image.hsv;

import ru.aptu.facedetector.image.Image;

/**
 * HSV color scheme.
 *
 */
public class HSVImage extends Image<HSVPixel> {  
	public HSVImage(HSVPixel[][] pixels) { 
		super(pixels); 
	}
}
