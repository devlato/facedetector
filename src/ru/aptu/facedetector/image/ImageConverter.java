// 
// 

package ru.aptu.facedetector.image; 

import ru.aptu.facedetector.image.bgra.BGRAImage;
import ru.aptu.facedetector.image.bgra.BGRAPixel;
import ru.aptu.facedetector.image.hsv.HSVImage;
import ru.aptu.facedetector.image.hsv.HSVPixel;
import ru.aptu.facedetector.image.integral.IntegralImage;

public class ImageConverter { 
	/**
	 * Converter from BGRA image presentation to a HSV image presentation. 
	 * @param    bgraImage   The image  presentation in BGRA format.
	 * @return   hsvImage    The image  presentation in HSV format.
	 */
	public HSVImage fromBGRAtoHSV(BGRAImage bgraImage) { 
		int width  = bgraImage.getWidth(); 
		int height = bgraImage.getHeight(); 
		BGRAPixel[][] bgraPixels = bgraImage.getPixels(); 
		HSVPixel[][] hsvPixels = new HSVPixel[height][width]; 
		HSVImage hsvImage = new HSVImage(hsvPixels); // hsvPixels points to an array, all is ok 
		
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) { 
				short red = bgraPixels[y][x].getRed(); 
				short green = bgraPixels[y][x].getGreen();
				short blue = bgraPixels[y][x].getBlue(); 
				short max = (short) Math.max(red, Math.max(green, blue)); 
				short min = (short) Math.min(red, Math.min(green, blue)); 
				// System.out.println("y, x = " + y + ", " + x); 
				
				short hue        = 0;
				short saturation = 0;
				short value      = 0; 
				if (max == min) { 
					hue = (short) 0; 
				} else if ((max == red) && (green >= blue)) {
					hue = (short) (60 * (green - blue) / (max - min)); 
				} else if ((max == red) && (blue > green)) {
					hue = (short) (60 * (green - blue) / (max - min) + 360); 
				} else if (max == green) {
					hue = (short) (60 * (blue - red) / (max - min) + 120); 
				} else if (max == blue) {
					hue = (short) (60 * (red - green) / (max - min) + 240);  
				}
				
				saturation = (max == 0) 
						? (short) 0 
						: (short) ((1 - min / max) * 100); 
				
				value = max; 
				
				hsvPixels[y][x] = new HSVPixel(hue, saturation, value); 
			} 
		} 
		return hsvImage;
	}
	
	public IntegralImage fromHSVImagetoIntegralImage(HSVImage hsvImage) {  
		int width  = hsvImage.getWidth(); 
		int height = hsvImage.getHeight(); 
		HSVPixel[][] hsvPixels = hsvImage.getPixels(); 
		Long[][] integralPixels = new Long[height][width]; 
		IntegralImage integralImage = new IntegralImage(integralPixels); 
		
		integralPixels[0][0] = (long) hsvPixels[0][0].getValue(); 
		for (int y = 1; y < height; y++) { 
			integralPixels[y][0] = integralPixels[y - 1][0] + hsvPixels[y][0].getValue(); 
		}
		for (int x = 1; x < width; x++) { 
			integralPixels[0][x] = integralPixels[0][x - 1] + hsvPixels[0][x].getValue(); 
		}
		for (int y = 1; y < height; y++) {
			for (int x = 1; x < width; x++) { 
				integralPixels[y][x] = integralPixels[y][x - 1] + 
						integralPixels[y - 1][x] - integralPixels[y - 1][x - 1] + 
						hsvPixels[y][x].getValue(); 
			}
		}
		
		return integralImage; 
	}
}
