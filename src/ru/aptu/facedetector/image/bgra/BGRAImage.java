/**
 * BGRAImage.java
 */

package ru.aptu.facedetector.image.bgra;

import ru.aptu.facedetector.image.Image;

public class BGRAImage extends Image<BGRAPixel> { 
	public BGRAImage(BGRAPixel[][] pixels) { 
		super(pixels); 
	}
}
