//
//

package ru.aptu.facedetector.image.bgra; 

public class BGRAPixel {
	private final short red; 
	private final short green; 
	private final short blue; 
	private final short alpha; 
	
	public BGRAPixel(short blue, short green, short red, short alpha) { 
		this.red   = red; 
		this.green = green; 
		this.blue  = blue; 
		this.alpha = alpha; 
	}
	
	public short getRed() {
		return red;
	}
	
	public short getGreen() {
		return green;
	}
	
	public short getBlue() {
		return blue;
	}
	
	public short getAlpha() {
		return alpha;
	}
	
	/**
	 * This methods performs a type cast 
	 * from BGRAPixel to String.
	 * @return     The String object containing field values.
	 */
	public String toString() {
		String string = new String(
			"{"          + 
			"Blue: "     + (int) this.blue  + ", " + 
			"Green: "    + (int) this.green + ", " + 
			"Red: "      + (int) this.red   + ", " + 
			"Reserved: " + (int) this.alpha + 
			"}"
		);
		return string;
	}
}
