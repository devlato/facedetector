package ru.aptu.facedetector.image.bgra;

import java.io.IOException;
import java.io.InputStream;

import ru.aptu.facedetector.DataTypeLoaderAndConverter;

public class BGRAPixelLoader {
	public BGRAPixel loadFromByteQuad(InputStream input) throws IOException { 
		return this.loadFromByteQuad((short) input.read(), (short) input.read(), (short) input.read(), (short) input.read()); 
	}
	
	public BGRAPixel loadFromByteQuad(short byte0, short byte1, short byte2, short byte3) { 
		return new BGRAPixel(byte0, byte1, byte2, byte3);  
	}
	
	public BGRAPixel loadFromByteTernary(InputStream input) throws IOException { 
		return this.loadFromByteTernary((short) input.read(), (short) input.read(), (short) input.read()); 
	}
	
	public BGRAPixel loadFromByteTernary(short byte0, short byte1, short byte2) { 
		return new BGRAPixel(byte0, byte1, byte2, (short) 0); 
	}
	
	public BGRAPixel loadFromBytePair(InputStream input) throws IOException { 
		return this.loadFromBytePair((short) input.read(), (short) input.read()); 
	}
	
	public BGRAPixel loadFromBytePair(short byte0, short byte1) { 
		short shortValue = DataTypeLoaderAndConverter.toShort((byte) byte0, (byte) byte1); 
		
		short blue  = (short) (shortValue & 0x001F); 
		short green = (short) (shortValue & 0x03E0); 
		short red   = (short) (shortValue & 0x7C00); 
		
		return new BGRAPixel(blue, green, red, (short) 0);  
	}
	
	public BGRAPixel loadFromByteUsingPalette(byte byte0, int bitsPerPixel, int index, BGRAPixel[] palette) { 
		int mask = (int) Math.pow(2, bitsPerPixel) - 1; 
		mask = mask << bitsPerPixel * index; 
		int paletteColor = byte0 & mask; 
		paletteColor = paletteColor >> bitsPerPixel * index; 
		
		// System.out.println("byte0 = " + byte0 + "\nmask = " + mask + "paletteColorIndex = " + paletteColor); 
		return palette[paletteColor];  
	}
}
