/**
 * IntegralImage.java
 */

package ru.aptu.facedetector.image.integral;

import ru.aptu.facedetector.image.Image;

/**
 * The class which used to generate and store 
 * an integral image data. 
 */
public class IntegralImage extends Image<Long> { 
	public IntegralImage(Long[][] pixels) { 
		super(pixels); 
	}
}
