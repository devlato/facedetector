// 
// 

package ru.aptu.facedetector.bitmap;

import java.io.IOException;
import java.io.InputStream;

import ru.aptu.facedetector.DataTypeLoaderAndConverter;

public class BitmapInfoHeaderLoader { 
	/**
	 * This method loads the data from the file
	 * to the appropriate fields.
	 * @param  input  BufferedInputStream object bound to the input file.
	 * @throws        IOException
	 * @see           BufferedInputStream
	 * @see           IOException
	 */
	public BitmapInfoHeader load(InputStream input) throws IOException { 
		return new BitmapInfoHeader(
				DataTypeLoaderAndConverter.loadLong(input), 
				DataTypeLoaderAndConverter.loadLong(input), 
				DataTypeLoaderAndConverter.loadLong(input), 
				DataTypeLoaderAndConverter.loadShort(input),  
				DataTypeLoaderAndConverter.loadShort(input),  
				DataTypeLoaderAndConverter.loadLong(input),  
				DataTypeLoaderAndConverter.loadLong(input),  
				DataTypeLoaderAndConverter.loadLong(input), 
				DataTypeLoaderAndConverter.loadLong(input), 
				DataTypeLoaderAndConverter.loadLong(input),  
				DataTypeLoaderAndConverter.loadLong(input)
		); 
	}
	
}
