package ru.aptu.facedetector.bitmap;

import java.io.IOException;
import java.io.InputStream;

import ru.aptu.facedetector.image.bgra.BGRAImage;
import ru.aptu.facedetector.image.bgra.BGRAPixel;
import ru.aptu.facedetector.image.bgra.BGRAPixelLoader;

public class BitmapLoader {
	/**
	 * This method loads the data from the file
	 * to the appropriate fields. 
	 * @throws     IOException
	 * @see        IOException
	 */
	public Bitmap load(InputStream input) throws IOException { 
		int fileSize = input.available(); 
		
		input.mark(fileSize); // Guaranteed to navigate through the entire file 
		Bitmap bitmap = new Bitmap();
		BitmapFileHeader fileHeader = new BitmapFileHeaderLoader().load(input);
		BitmapInfoHeader infoHeader = new BitmapInfoHeaderLoader().load(input); 
		bitmap.setFileHeader(fileHeader); 
		bitmap.setInfoHeader(infoHeader); 
		
		long pixelDataStartingPosition = fileHeader.getBitmapPixelDataStartingPosition(); 
		
		int imageWidth  = (int) infoHeader.getImageWidth(); 
		int imageHeight = (int) infoHeader.getImageHeight(); 
		
		int bitsPerPixel = infoHeader.getBitsPerPixelCount(); 
		boolean bmpHasPalette = (bitsPerPixel < 16); 
		
		BGRAPixel[][] pixels = new BGRAPixel[imageHeight][imageWidth]; 
		BGRAPixelLoader pixelLoader = new BGRAPixelLoader(); 
		if (bmpHasPalette) { 
			int paletteColorAmount = (int) Math.pow(2, bitsPerPixel); 
			BGRAPixel[] palette = new BGRAPixel[paletteColorAmount]; 
			for (int i = 0; i < paletteColorAmount; i++) { 
				palette[i] = pixelLoader.loadFromByteQuad(input); 
			}
			input.skip(1024 - paletteColorAmount * 4); 
			
			int pixelsPerByte = 8 / bitsPerPixel; 
			short currentByte = 0; 
			for (int counter = 0, y = 0, x = 0, byteInternalPosition = 0; counter < imageWidth * imageHeight; counter++) { 
				y = imageHeight - counter / imageWidth - 1; 
				x = counter % imageWidth; 
				byteInternalPosition = counter % pixelsPerByte; 
				if ((x == 0) && (y != imageHeight - 1)) { 
					input.skip((4 - (imageWidth / pixelsPerByte) % 4) % 4); 
				}
				if (byteInternalPosition == 0) { 
					currentByte = (short) input.read(); 
				}
				pixels[y][x] = pixelLoader.loadFromByteUsingPalette((byte) currentByte, bitsPerPixel, byteInternalPosition, palette); 
			}
			
		} else { 
			input.reset(); 
			// System.out.println("available = " + input.available()); 
			input.skip(pixelDataStartingPosition);
			// System.out.println("available = " + input.available()); 
			
			for (int y = imageHeight - 1; y >= 0; y--) { 
				for (int x = 0; x < imageWidth; x++) { 
					if (bitsPerPixel == 16) { 
						pixels[y][x] = pixelLoader.loadFromBytePair(input); 
					} else if (bitsPerPixel == 24) { 
						pixels[y][x] = pixelLoader.loadFromByteTernary(input);
					} else { 
						pixels[y][x] = pixelLoader.loadFromByteQuad(input);
					}
					// if (pixels[y][x].getBlue() != 64) System.out.println(pixels[y][x]); 
				}
				if (bitsPerPixel == 16) { 
					input.skip((3 * imageWidth) % 4); 
				}
				else if (bitsPerPixel == 24) { 
					input.skip((2 * imageWidth) % 4); 
				}
			}
		}
		bitmap.setBGRAImage(new BGRAImage(pixels)); 
		
		return bitmap; 
	}
}
