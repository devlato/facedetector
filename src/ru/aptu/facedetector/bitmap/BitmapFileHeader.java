/**
 * BitmapFileHeader.java
 */

package ru.aptu.facedetector.bitmap;

/**
 * The class containing the data 
 * of the BMP format BITMAPFILEHEADER structure.
 */
class BitmapFileHeader { 
	/**
	 * Bitmap type.
	 */
	private final String bfType; 
	
	/**
	 * Total File Size.
	 */
	private final long bfSize;
	
	/**
	 * Reserved word.
	 */
	private final int bfReserved1;
	
	/**
	 * Reserved word.
	 */
	private final int bfReserved2;
	
	/**
	 * Pixel data starting position.
	 */
	private final long bfOffBits;
	
	public BitmapFileHeader(String bitmapType, long totalSize, 
			int reservedWord1, int reservedWord2, 
			long pixelDataStartingPosition) { 
		this.bfType      = bitmapType; 
		this.bfSize      = totalSize; 
		this.bfReserved1 = reservedWord1; 
		
		this.bfReserved2 = reservedWord2;
		this.bfOffBits   = pixelDataStartingPosition;
	}
	
	/**
	 * Getter for the bitmap type
	 * The getter has an official documentation-oriented name.
	 */
	public String getBFType() {
		return this.bfType; 
	}
	
	/**
	 * Getter for the total bitmap file size.
	 * The getter has an official documentation-oriented name
	 */
	public long getBFSize() {
		return this.bfSize; 
	}
	
	/**
	 * Getter for the reserved word #1.
	 * The getter has an official documentation-oriented name
	 */
	public int getBFReserved1() {
		return this.bfReserved1; 
	}
	
	/**
	 * Getter for the reserved word #2.
	 * The getter has an official documentation-oriented name
	 */
	public int getBFReserved2() {
		return this.bfReserved2; 
	}
	
	/**
	 * Getter for the pixel data offset from the file start.
	 * The getter has an official documentation-oriented name.
	 */
	public long getBFOffBits() { 
		return this.bfOffBits; 
	}
	
	/**
	 * Getter for the bitmap type.
	 * The getter has an official documentation-oriented name.
	 */
	public String getBitmapType() {
		return this.bfType; 
	}
	
	/**
	 * Getter for the total bitmap file size.
	 * The getter has user-friendly name.
	 */
	public long getTotalBitmapFileSize() { 
		return this.bfSize; 
	}
	
	/**
	 * Getter for the reserved word #1.
	 * The getter has user-friendly name.
	 */
	public int getReservedWord1() {
		return this.bfReserved1; 
	}
	
	/**
	 * Getter for the reserved word #2.
	 * The getter has user-friendly name.
	 */
	public int getReservedWord2() {
		return this.bfReserved2; 
	}
	
	/**
	 * Getter for the pixel data offset from the file start.
	 * The getter has user-friendly name.
	 */
	public long getBitmapPixelDataStartingPosition() { 
		return this.bfOffBits; 
	}
	
	
	/**
	 * This methods performs a type cast 
	 * from BitmapFileHeader to String.
	 * @return     The String object containing field values.
	 */
	public String toString() { 
		String newLine = System.getProperty("line.separator");
		String string = new String(
			"bfType      = " + this.bfType      + newLine + 
			"bfSize      = " + this.bfSize      + newLine +  
			"bfReserved1 = " + this.bfReserved1 + newLine + 
			"bfReserved2 = " + this.bfReserved2 + newLine + 
			"bfOffBits   = " + this.bfOffBits
		);
		return string;	
	}
}
