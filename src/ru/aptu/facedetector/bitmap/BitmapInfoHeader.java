/**
 * BitmapInfoHeader.java
 */

package ru.aptu.facedetector.bitmap;

/**
 * The class containing the data 
 * of the BMP format BITMAPINFOHEADER structure.
 */
class BitmapInfoHeader {
	/** 
	 * Size of the structure.
	 */
	private final long biSize; 
	
	/** 
	 * The width of the image in pixels.
	 */
	private final long biWidth;
	
	/** 
	 * The height of the image in pixels.
	 */
	private final long biHeight; 
	
	/** 
	 * The amount of the planes in the image.
	 */
	private final int biPlanes; 
	
	/** 
	 * Quantity of bits per pixel.
	 */
	private final int biBitCount;
	
	/** 
	 * Compression type if it was used.
	 */
	private final long biCompression; 
	
	/** 
	 * Image size (should be zero if there is no 
	 * compression used).
	 */
	private final long biSizeImage; 
	
	/** 
	 * Amount of pixels per meter horizontally 
	 * (for printing etc.).
	 */
	private final long biXPelsPerMeter; 
	
	/** 
	 * Amount of pixels per meter vertically 
	 * (for printing etc.).
	 */
	private final long biYPelsPerMeter;  
	
	/** 
	 * Amount of the table colors that was used 
	 * (for compressed images only).
	 */
	private final long biClrUsed; 
	
	/** 
	 * Amount of important colors that was used 
	 * (0 => all colors are important).
	 */
	private final long biClrImportant;
	
	public BitmapInfoHeader(long structureSize, long width, long height, 
			int planesCount, int bitsPerPixelCount, long compressionType, 
			long imageSize, long pixelsPerMeterX, long pixelsPerMeterY, 
			long usedColorCount, long importantColorCount) { 
		this.biSize          = structureSize; 
		this.biWidth         = width;
		this.biHeight        = height; 
		this.biPlanes        = planesCount; 
		this.biBitCount      = bitsPerPixelCount;
		this.biCompression   = compressionType; 
		this.biSizeImage     = imageSize; 
		this.biXPelsPerMeter = pixelsPerMeterX; 
		this.biYPelsPerMeter = pixelsPerMeterY;  
		this.biClrUsed       = usedColorCount; 
		this.biClrImportant  = importantColorCount;
	}
	
	/**
	 * Getter for the size of structure.
	 * The getter has an official documentation-oriented name
	 */
	public long getBISize() { 
		return this.biSize; 
	}
	
	/**
	 * Getter for the image width in pixels.
	 * The getter has an official documentation-oriented name
	 */
	public long getBIWidth() { 
		return this.biWidth; 
	}
	
	/**
	 * Getter for the image height in pixels.
	 * The getter has an official documentation-oriented name
	 */
	public long getBIHeight() { 
		return this.biHeight; 
	}
	
	/**
	 * Getter for the image planes amount.
	 * The getter has an official documentation-oriented name
	 */
	public int getBIPlanes() { 
		return this.biPlanes; 
	}
	
	/**
	 * Getter for the quantity of bits per pixel.
	 * The getter has an official documentation-oriented name
	 */
	public int getBIBitCount() { 
		return this.biBitCount; 
	}
	
	/**
	 * Getter for the compression type if it was used.
	 * The getter has an official documentation-oriented name
	 */
	public long getBICompression() { 
		return this.biCompression; 
	}
	
	/**
	 * Getter for the image size in bytes 
	 * (not really, this field usually contains zero 
	 * if there is no compression used).
	 * The getter has an official documentation-oriented name
	 */
	public long getBISizeImage() { 
		return this.biSizeImage; 
	}
	
	/**
	 * Getter for the amount of pixels per meter horizontally 
	 * (may be used for printing tasks etc.).
	 * The getter has an official documentation-oriented name
	 */
	public long getBIXPelsPerMeter() { 
		return this.biXPelsPerMeter; 
	}
	
	/**
	 * Getter for the amount of pixels per meter vertically 
	 * (may be used for printing tasks etc.).
	 * The getter has an official documentation-oriented name
	 */
	public long getBIYPelsPerMeter() { 
		return this.biYPelsPerMeter; 
	}
	
	/**
	 * Getter for the amount of colors used 
	 * (non-zero only if compression was used).
	 * The getter has an official documentation-oriented name
	 */
	public long getBIClrUsed() { 
		return this.biClrUsed; 
	}
	
	/**
	 * Getter for the amount of colors which are important
	 * (usually set to zero).
	 * The getter has an official documentation-oriented name
	 */
	public long getBIClrImportant() { 
		return this.biClrImportant; 
	}
	
	
	/**
	 * Getter for the size of structure.
	 * The getter has user-friendly name.
	 */
	public long getStructureSize() { 
		return this.biSize; 
	}
	
	/**
	 * Getter for the image width in pixels. 
	 * The getter has user-friendly name.
	 */
	public long getImageWidth() { 
		return this.biWidth; 
	}
	
	/**
	 * Getter for the image height in pixels.
	 * The getter has user-friendly name.
	 */
	public long getImageHeight() { 
		return this.biHeight; 
	}
	
	/**
	 * Getter for the image planes amount.
	 * The getter has user-friendly name.
	 */
	public int getImagePlaneCount() { 
		return this.biPlanes; 
	}
	
	/**
	 * Getter for the quantity of bits per pixel.
	 * The getter has user-friendly name.
	 */
	public int getBitsPerPixelCount() { 
		return this.biBitCount; 
	}
	
	/**
	 * Getter for the compression type if it was used.
	 * The getter has user-friendly name.
	 */
	public long getCompressionType() { 
		return this.biCompression; 
	}
	
	/**
	 * Getter for the image size in bytes 
	 * (not really, this field usually contains zero 
	 * if there is no compression used).
	 * The getter has user-friendly name.
	 */
	public long getActialImageSize() { 
		return this.biSizeImage; 
	}
	
	/**
	 * Getter for the amount of pixels per meter horizontally 
	 * (may be used for printing tasks etc.).
	 * The getter has user-friendly name.
	 */
	public long getXPixelsPerMeter() { 
		return this.biXPelsPerMeter; 
	}
	
	/**
	 * Getter for the amount of pixels per meter vertically 
	 * (may be used for printing tasks etc.).
	 * The getter has user-friendly name.
	 */
	public long getYPixelsPerMeter() { 
		return this.biYPelsPerMeter; 
	}
	
	/**
	 * Getter for the amount of colors used 
	 * (non-zero only if compression was used).
	 * The getter has user-friendly name.
	 */
	public long getUsedColorsCount() { 
		return this.biClrUsed; 
	}
	
	/**
	 * Getter for the amount of colors which are important
	 * (usually set to zero).
	 * The getter has user-friendly name.
	 */
	public long getImportantColorsAmount() { 
		return this.biClrImportant; 
	}
	
	
	/**
	 * This methods performs a type cast 
	 * from BitmapFileHeader to String.
	 * @return     The String object containing field values.
	 */
	public String toString() { 
		String newLine = System.getProperty("line.separator");
		String string = new String(
			"biSize          = " + this.biSize          + newLine + 
			"biWidth         = " + this.biWidth         + newLine + 
			"biHeight        = " + this.biHeight        + newLine + 
			"biPlanes        = " + this.biPlanes        + newLine + 
			"biBitCount      = " + this.biBitCount      + newLine + 
			"biCompression   = " + this.biCompression   + newLine + 
			"biSizeImage     = " + this.biSizeImage     + newLine + 
			"biXPelsPerMeter = " + this.biXPelsPerMeter + newLine + 
			"biYPelsPerMeter = " + this.biYPelsPerMeter + newLine + 
			"biClrUsed       = " + this.biClrUsed       + newLine + 
			"biClrImportant  = " + this.biClrImportant
		);
		return string; 
	}
}
