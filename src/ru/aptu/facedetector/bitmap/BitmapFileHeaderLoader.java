/**
 * BitmapFileHeaderLoader.java
 */

package ru.aptu.facedetector.bitmap;

import java.io.IOException;
import java.io.InputStream;

import ru.aptu.facedetector.DataTypeLoaderAndConverter;

public class BitmapFileHeaderLoader { 
	/**
	 * This method loads the data from the file
	 * to the appropriate fields.
	 * @param  in  BufferedInputStream object bound to the input file.
	 * @throws     IOException
	 * @see        BufferedInputStream
	 * @see        IOException
	 */
	public BitmapFileHeader load(InputStream input) throws IOException { 
		return new BitmapFileHeader(
				DataTypeLoaderAndConverter.loadANSIString(input, 2), 
				DataTypeLoaderAndConverter.loadLong(input), 
				DataTypeLoaderAndConverter.loadShort(input),  
				DataTypeLoaderAndConverter.loadShort(input), 
				DataTypeLoaderAndConverter.loadLong(input)
		);  
	}
}
