/**
 * BMP.java
 */

package ru.aptu.facedetector.bitmap;

import ru.aptu.facedetector.image.bgra.BGRAImage;

/**
 * This is the class containing the data of the Bitmap structure.
 */
public class Bitmap {   
	/**
	 * Bitmap File Header 
	 * (contains BITMAPFILEHEADER structure data), 
	 * see BMP format description for info about it. 
	 */
	private BitmapFileHeader fileHeader; 
	
	/**
	 * Bitmap Info Header 
	 * (contains BITMAPINFOHEADER structure data), 
	 * see BMP format description for info about it. 
	 */
	private BitmapInfoHeader infoHeader;
	
	/**
	 * Bitmap pixel data — a 2D array of pixel objects. 
	 */
	private BGRAImage pixels; 
	
	
	/**
	 * Getter for the pixels array.
	 */
	public BGRAImage getBGRAImage() { 
		return this.pixels; 
	} 
	
	public void setBGRAImage(BGRAImage pixels) { 
		this.pixels = pixels; 
	}
	
	public BitmapFileHeader getFileHeader() { 
		return this.fileHeader; 
	}
	
	public void setFileHeader(BitmapFileHeader header) { 
		this.fileHeader = header; 
	}
	
	public BitmapInfoHeader getInfoHeader() { 
		return this.infoHeader; 
	}
	
	public void setInfoHeader(BitmapInfoHeader header) { 
		this.infoHeader = header; 
	}
	
	/**
	 * This methods performs a type cast 
	 * from BitmapFileHeader to String.
	 * @return     The String object containing field values.
	 */
	public String toString() {
		String newLine = System.getProperty("line.separator");
		String string = new String(
			"Bitmap Type: "                 + newLine + 
			this.getBitmapTypeDescription() + newLine + 
			"Bitmap File Header: "          + newLine + 
			this.fileHeader.toString()      + newLine +
			"Bitmap Info Header: "          + newLine + 
			this.infoHeader.toString()
		);
		return string;
	}
	
	/**
	 * The method used to get a bitmap type description string.
	 * @return   The String object containing a type description. 
	 * @see      String 
	 */
	public String getBitmapTypeDescription() {
		String bitmapTypeDescription = new String(); 
		String bitmapType = this.fileHeader.getBitmapType(); 
		
		if ("BM".equals(bitmapType)) {
			bitmapTypeDescription = "Windows bitmap"; 
		}
		else if ("BA".equals(bitmapType)) {
			bitmapTypeDescription = "OS/2 bitmap array";
		}
		else if ("CI".equals(bitmapType)) {
			bitmapTypeDescription = "OS/2 color icon";
		}
		else if ("CP".equals(bitmapType)) { 
			bitmapTypeDescription = "OS/2 colro pointer";
		}
		else if ("IC".equals(bitmapType)) { 
			bitmapTypeDescription = "OS/2 icon";
		}
		else if ("IT".equals(bitmapType)) {
			bitmapTypeDescription = "OS/2 pointer";
		}
		else {
			bitmapTypeDescription = "Unknown type, may be not BMP";
		}
		
		return bitmapTypeDescription; 
	}
}
