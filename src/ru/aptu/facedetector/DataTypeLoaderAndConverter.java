/**
 * Loader.java
 */

package ru.aptu.facedetector;

import java.io.IOException;
import java.io.InputStream;

/**
 * The class containing some static methods used to load 
 * and convert data using a given BufferedInputStream instance. 
 */
public final class DataTypeLoaderAndConverter { 
	/**
	 * The method used to cast a byte sequence to a variable of a long type.
	 * @param   b0  0th byte.
	 * @param   b1  1st byte.
	 * @param   b2  2nd byte.
	 * @param   b3  3rd byte. 
	 * @return      The value of a long type. 
	 * @see         long 
	 * @see         byte
	 */
	public static long toLong(byte b0, byte b1, byte b2, byte b3) {
		return toLong(toShort(b0, b1), toShort(b2, b3));
	}
	
	/**
	 * The method used to cast a short numbers sequence to a variable of a long type.
	 * @param   b0  0th short number.
	 * @param   b1  1st short number. 
	 * @return      The value of a long type. 
	 * @see         long 
	 * @see         short
	 */
	public static long toLong(short s0, short s1) {
		long l0 = s0 & 0xffff;
		long l1 = s1 & 0xffff;
		return (l1 << 16) | l0;
	}
	
	/**
	 * The method used to convert a byte sequence to a variable of a short type.
	 * @param   b0  0th byte.
	 * @param   b1  1st byte.
	 * @return      The value of a short type. 
	 * @see         short 
	 * @see         byte
	 */
	public static short toShort(byte b0, byte b1) {
		long l0 = b0 & 0xff;
		long l1 = b1 & 0xff;
		return (short)((l1 << 8) | l0);
	}
	
	/**
	 * The method used to load a long variable from the input stream 
	 * using the instance of BufferedInputStream.
	 * @param   in  The instance of BufferedInputStream.
	 * @return      The value of a long type. 
	 * @see         BufferedInputStream 
	 * @see         long
	 */
	public static long loadLong(InputStream in) throws IOException { 
		return toLong((byte) in.read(), (byte) in.read(), (byte) in.read(), (byte) in.read());
	}
	
	/**
	 * The method used to load a short variable from the input stream 
	 * using the instance of BufferedInputStream.
	 * @param   in  The instance of BufferedInputStream.
	 * @return      The value of a short type. 
	 * @see         BufferedInputStream 
	 * @see         short
	 */
	public static short loadShort(InputStream in) throws IOException {
		return toShort((byte) in.read(), (byte) in.read());
	}
	
	/**
	 * The method used to load a single-byte character from the input stream 
	 * using the instance of BufferedInputStream.
	 * @param   in  The instance of BufferedInputStream.
	 * @return      The character. 
	 * @see         BufferedInputStream 
	 * @see         char
	 */
	public static char loadANSIChar(InputStream in) throws IOException { 
		return (char) in.read(); 
	}
	
	/**
	 * The method used to load a single-byte character string from the input stream 
	 * using the instance of BufferedInputStream.
	 * @param   in  The instance of BufferedInputStream.
	 * @return      The requested string. 
	 * @see         BufferedInputStream 
	 * @see         String
	 */
	public static String loadANSIString(InputStream in, int length) throws IOException {
		String result = new String(); 
		byte[] readData = new byte[length]; 
		in.read(readData, 0, length); 
		for (int i = 0; i < length; i++) {
			result += (char) readData[i]; 
		}
		return result; 
	}
}
