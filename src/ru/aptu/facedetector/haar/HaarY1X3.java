package ru.aptu.facedetector.haar;

import ru.aptu.facedetector.image.integral.IntegralImage;

public class HaarY1X3 extends HaarFeatureCalculator {

	public HaarY1X3(int windowSizeY, int windowSizeX) {
		super(windowSizeY, windowSizeX);
	}
	
	public HaarY1X3(int windowSizeY, int windowSizeX, long threshold) { 
		super(windowSizeY, windowSizeX, threshold);
	}
	
	public long calculate(int positionY, int positionX, IntegralImage integralImage) { 
		long leftPartSum   = 0;
		long rightPartSum  = 0;
		long middlePartSum = 0; 
		int third = this.windowSizeX / 3; 
		Long[][] integralPixels = integralImage.getPixels(); 
		
		// Left path: 
		// top left:     (positionY, positionX) 
		// bottom left:  (positionY + windowSizeY, positionX) 
		// top right:    (positionY, positionX + third) 
		// bottom right: (positionY + windowSizeY, positionX + third) 
		leftPartSum = integralPixels[positionY + windowSizeY][positionX + third] - 
				integralPixels[positionY][positionX + third] - 
				integralPixels[positionY + windowSizeY][positionX] + 
				integralPixels[positionY][positionX]; 
		
		// Middle path: 
		// top left:     (positionY, positionX + third) 
		// bottom left:  (positionY + windowSizeY, positionX + third) 
		// top right:    (positionY, positionX + 2 * third) 
		// bottom right: (positionY + windowSizeY, positionX + 2 * third) 
		middlePartSum = integralPixels[positionY + windowSizeY][positionX + 2 * third] - 
				integralPixels[positionY][positionX + 2 * third] - 
				integralPixels[positionY + windowSizeY][positionX + third] + 
				integralPixels[positionY][positionX + third]; 
		
		// Right path: 
		// top left:     (positionY, positionX + 2 * third) 
		// bottom left:  (positionY + windowSizeY, positionX + 2 * third) 
		// top right:    (positionY, positionX + windowSizeX) 
		// bottom right: (positionY + windowSizeY, positionX + windowSizeX) 
		rightPartSum = integralPixels[positionY + windowSizeY][positionX + windowSizeX] - 
				integralPixels[positionY][positionX + windowSizeX] - 
				integralPixels[positionY + windowSizeY][positionX + 2 * third] + 
				integralPixels[positionY][positionX + 2 * third]; 
		
		return (2 * middlePartSum - leftPartSum - rightPartSum);
	}
	
}
