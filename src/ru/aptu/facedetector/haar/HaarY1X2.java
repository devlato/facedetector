package ru.aptu.facedetector.haar;

import ru.aptu.facedetector.image.integral.IntegralImage;

public class HaarY1X2 extends HaarFeatureCalculator {
	
	public HaarY1X2(int windowSizeY, int windowSizeX) {
		super(windowSizeY, windowSizeX);
	}
	
	public HaarY1X2(int windowSizeY, int windowSizeX, long threshold) { 
		super(windowSizeY, windowSizeX, threshold);
	}

	public long calculate(int positionY, int positionX, IntegralImage integralImage) { 
		long leftPartSum = 0;
		long rightPartSum = 0; 
		int middleX = this.windowSizeX / 2; 
		Long[][] integralPixels = integralImage.getPixels(); 
		
		// Left path: 
		// top left:     (positionY, positionX) 
		// bottom left:  (positionY + windowSizeY, positionX) 
		// top right:    (positionY, positionX + middleX) 
		// bottom right: (positionY + windowSizeY, positionX + middleX) 
		leftPartSum = integralPixels[positionY + windowSizeY][positionX + middleX] - 
				integralPixels[positionY][positionX + middleX] - 
				integralPixels[positionY + windowSizeY][positionX] + 
				integralPixels[positionY][positionX]; 
		
		// Right part: 
		// top left:     (positionY, positionX + middleX) 
		// bottom left:  (positionY + windowSizeY, positionX + middleX) 
		// top right:    (positionY, positionX + windowSizeX) 
		// bottom right: (positionY + windowSizeY, positionX + windowSizeX) 
		rightPartSum = integralPixels[positionY + windowSizeY][positionX + windowSizeX] - 
				integralPixels[positionY][positionX + windowSizeX] - 
				integralPixels[positionY + windowSizeY][positionX + middleX] + 
				integralPixels[positionY][positionX + middleX]; 
		
		return (leftPartSum - rightPartSum);  
	}
	
}
