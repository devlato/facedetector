// 
// 

package ru.aptu.facedetector.haar;

import ru.aptu.facedetector.image.integral.IntegralImage;

public abstract class HaarFeatureCalculator {
	protected int windowSizeY; 
	protected int windowSizeX; 
	private long threshold; 
	private long positiveThreshold; 
	private long falsePositiveThreshold;  
	
	public HaarFeatureCalculator(int windowSizeY, int windowSizeX) { 
		this(windowSizeY, windowSizeX, Long.MAX_VALUE); 
	}
	
	public HaarFeatureCalculator(int windowSizeY, int windowSizeX, long threshold) { 
		this.setWindowSize(windowSizeY, windowSizeX); 
		this.threshold = threshold; 
		this.positiveThreshold = threshold;
		this.falsePositiveThreshold = 0; 
	}
	
	public long getThreshold() { 
		return this.threshold; 
	}
	
	public void setThreshold(long threshold) { 
		this.threshold = threshold; 
	}
	
	public void setThreshold(long positiveThreshold, long falsePositiveThreshold) {
		this.positiveThreshold = positiveThreshold;
		this.falsePositiveThreshold = falsePositiveThreshold; 
		this.threshold = positiveThreshold - falsePositiveThreshold; 
	}
	
	public void setPositiveThreshold(long threshold) { 
		this.positiveThreshold = threshold; 
	}
	
	public void setFalsePositiveThreshold(long threshold) { 
		this.falsePositiveThreshold = threshold; 
	}
	
	public long getPositiveThreshold() { 
		return this.positiveThreshold; 
	}
	
	public long getFalsePositiveThreshold() { 
		return this.falsePositiveThreshold; 
	}
	
	public int getWindowSizeX() {
		return windowSizeX;
	}

	public void setWindowSizeX(int windowSizeX) {
		this.windowSizeX = windowSizeX;
	}

	public int getWindowSizeY() {
		return windowSizeY; 
	}

	public void setWindowSizeY(int windowSizeY) {
		this.windowSizeY = windowSizeY; 
	}
	
	public void setWindowSize(int windowSizeY, int windowSizeX) { 
		this.windowSizeY = windowSizeY; 
		this.windowSizeX = windowSizeX; 
	}
	
	public void setMainParams(int windowSizeY, int windowSizeX) { 
		this.setMainParams(windowSizeY, windowSizeX, this.threshold); 
	}
	
	public void setMainParams(int windowSizeY, int windowSizeX, long threshold) { 
		this.windowSizeY = windowSizeY; 
		this.windowSizeX = windowSizeX;  
		this.threshold = threshold; 
	}
	
	public abstract long calculate(int positionY, int positionX, IntegralImage integralImage); 
	
}
