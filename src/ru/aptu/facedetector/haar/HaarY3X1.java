package ru.aptu.facedetector.haar;

import ru.aptu.facedetector.image.integral.IntegralImage;

public class HaarY3X1 extends HaarFeatureCalculator {

	public HaarY3X1(int windowSizeY, int windowSizeX) {
		super(windowSizeY, windowSizeX);
	}
	
	public HaarY3X1(int windowSizeY, int windowSizeX, long threshold) { 
		super(windowSizeY, windowSizeX, threshold);
	}

	public long calculate(int positionY, int positionX, IntegralImage integralImage) { 
		long topPartSum    = 0;
		long bottomPartSum = 0;
		long middlePartSum = 0; 
		int third = this.windowSizeY / 3; 
		Long[][] integralPixels = integralImage.getPixels(); 
		
		// Top path: 
		// top left:     (positionY, positionX) 
		// bottom left:  (positionY + third, positionX) 
		// top right:    (positionY, positionX + windowSizeX) 
		// bottom right: (positionY + third, positionX + windowSizeX) 
		topPartSum = integralPixels[positionY + third][positionX + windowSizeX] - 
				integralPixels[positionY][positionX + windowSizeX] - 
				integralPixels[positionY + third][positionX] + 
				integralPixels[positionY][positionX]; 
		
		// Middle path: 
		// top left:     (positionY + third, positionX) 
		// bottom left:  (positionY + 2 * third, positionX) 
		// top right:    (positionY + third, positionX + windowSizeX) 
		// bottom right: (positionY + 2 * third, positionX + windowSizeX) 
		middlePartSum = integralPixels[positionY + third][positionX] - 
				integralPixels[positionY + 2 * third][positionX] - 
				integralPixels[positionY + third][positionX + windowSizeX] + 
				integralPixels[positionY + 2 * third][positionX + windowSizeX]; 
		
		// Bottom path: 
		// top left:     (positionY + 2 * third, positionX) 
		// bottom left:  (positionY + windowSizeY, positionX) 
		// top right:    (positionY + 2 * third, positionX + windowSizeX) 
		// bottom right: (positionY + windowSizeY, positionX + windowSizeX) 
		bottomPartSum = integralPixels[positionY + windowSizeY][positionX + windowSizeX] - 
				integralPixels[positionY + 2 * third][positionX + windowSizeX] - 
				integralPixels[positionY + windowSizeY][positionX] + 
				integralPixels[positionY + 2 * third][positionX]; 
		
		return (2 * middlePartSum - topPartSum - bottomPartSum); 
	}
	
}
