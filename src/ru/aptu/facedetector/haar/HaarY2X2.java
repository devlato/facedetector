package ru.aptu.facedetector.haar;

import ru.aptu.facedetector.image.integral.IntegralImage;

public class HaarY2X2 extends HaarFeatureCalculator {
	
	public HaarY2X2(int windowSizeY, int windowSizeX) {
		super(windowSizeY, windowSizeX);
	}
	
	public HaarY2X2(int windowSizeY, int windowSizeX, long threshold) { 
		super(windowSizeY, windowSizeX, threshold);
	}
	
	public long calculate(int positionY, int positionX, IntegralImage integralImage) { 
		HaarY1X2 haarY1X2 = new HaarY1X2(this.windowSizeY, this.windowSizeX); 
		long topPartSum    = 0;
		long bottomPartSum = 0; 
		int middleY = this.windowSizeY / 2;
		
		topPartSum = haarY1X2.calculate(positionY, positionX, integralImage); 
		bottomPartSum = haarY1X2.calculate(positionY - middleY, positionX, integralImage);  
		
		return (topPartSum - bottomPartSum); 
	}
	
}
