// 
// 

package ru.aptu.facedetector.haar;

import ru.aptu.facedetector.image.integral.IntegralImage;

public class HaarY2X1 extends HaarFeatureCalculator {
	
	public HaarY2X1(int windowSizeY, int windowSizeX) {
		super(windowSizeY, windowSizeX);
	}
	
	public HaarY2X1(int windowSizeY, int windowSizeX, long threshold) { 
		super(windowSizeY, windowSizeX, threshold);
	}
	
	public long calculate(int positionY, int positionX, IntegralImage integralImage) { 
		long topPartSum = 0;
		long bottomPartSum = 0; 
		int middleY = this.windowSizeY / 2; 
		Long[][] integralPixels = integralImage.getPixels(); 
		
		// Top path: 
		// top left:     (positionY, positionX) 
		// bottom left:  (positionY + middleY, positionX) 
		// top right:    (positionY, positionX + windowSizeX) 
		// bottom right: (positionY + middleY, positionX + windowSizeX) 
		topPartSum = integralPixels[positionY + middleY][positionX + this.windowSizeX] - 
				integralPixels[positionY + middleY][positionX] - 
				integralPixels[positionY][positionX + this.windowSizeX] + 
				integralPixels[positionY][positionX]; 
		
		// Bottom part: 
		// top left:     (positionY + middleY + 1, positionX) 
		// bottom left:  (positionY + windowSizeY, positionX) 
		// top right:    (positionY + middleY + 1, positionX + windowSizeX) 
		// bottom right: (positionY + windowSizeY, positionX + windowSizeX) 
		bottomPartSum = integralPixels[positionY + this.windowSizeY][positionX + this.windowSizeX] - 
				integralPixels[positionY + this.windowSizeY][positionX] - 
				integralPixels[positionY + middleY][positionX + this.windowSizeX] + 
				integralPixels[positionY + middleY][positionX]; 
		
		return (topPartSum - bottomPartSum);  
	}
	
}
