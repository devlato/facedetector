// 
// 

package ru.aptu.facedetector.haar;

public class HaarFeature { 
	private Long[][] pixels;
	
	public HaarFeature(Long[][] pixels) { 
		this.pixels = pixels; 
	}

	public int getHeight() { 
		if (this.pixels == null) { 
			return 0; 
		} else { 
			return this.pixels.length; 
		}
	}
	
	public int getWidth() { 
		if (this.getHeight() == 0) { 
			return 0; 
		} else { 
			return this.pixels[0].length; 
		}
	}
	
	public Long[][] getPixels() { 
		return this.pixels; 
	}
	
	public String toString() { 
		String newLine = System.getProperty("line.separator"); 
		return new String( 
			"width: "  + this.getWidth()  + newLine + 
			"height: " + this.getHeight() + newLine
		);  
	}
	
	public void fullPrint() { 
		System.out.println("Full data = {"); 
		for (int y = 0; y < this.getHeight(); y++) { 
			for (int x = 0; x < this.getWidth(); x++) { 
				System.out.print(this.pixels[y][x] + "\t"); 
			}
			System.out.println(""); 
		}
		System.out.println("}"); 
	}
}
